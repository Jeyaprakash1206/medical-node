'use strict';

// Load modules
var Mongoose = require('mongoose')

// Mongoose connection url 
Mongoose.Promise = global.Promise;
Mongoose.connect('mongodb://localhost/Login');

// Registering Connection and Connecting with MongoDb
var db = Mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function callback() {
    console.log("Connection with database succeeded.");
});

exports.Mongoose = Mongoose;
exports.db = db;