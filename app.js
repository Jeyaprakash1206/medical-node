'use strict';

// Library Node files loaded
var Hapi = require('hapi'),
    CookieAuth = require('hapi-auth-cookie');

// Project Node files loaded
var Routes = require('./route'),
    Db = require('./db');

// Creating node Hapi.js Server.
var server = new Hapi.Server();

// Connecting to created Hapi.js Server
server.connection({
    port: 8002,
    routes: {
        cors: {
            origin: ['*']
        }
    }
});

var validate = {
    password: 'Hapi-Auth-Cookie-Session-Secrect',
    cookie: 'session',
    redirectTo: false,
    isSecure: true,
    ttl: 24 * 60 * 60 * 1000 // Set session to 1 day
}
// register plugins to server instance
server.register(CookieAuth, function (err) {
    if (err) {
        throw err;
    }
    server.auth.strategy('session', 'cookie', validate);
    // Defining api routes
    server.route(Routes);
});

// Starting Server up & Running
server.start(function () {
    console.log('Server started ', server.info.uri);
});