'use strict';

var authCtrl = require('./controllers/authCtrl');

var Routes = [

    { path: '/createNewUser', method: 'POST', handler: authCtrl.createNewUser },
    { path: '/authenticate', method: 'POST', handler: authCtrl.loginValidate },
    {
        path: '/updateProfile', method: 'POST', handler: authCtrl.UpdateProfile,
        config: { auth: { strategy: 'session' } }
    }
];

module.exports = Routes;
