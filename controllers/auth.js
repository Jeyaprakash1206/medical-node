var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CreateUser = new Schema({
    FirstName: {
        type: String,
        required: true
    },
    LastName: {
        type: String,
        required: true
    },
    Email: {
        type: String,
        required: true,
        unique: true
    },
    Password: {
        type: String,
        required: true
    },
    PhoneNumber: {
        type: Number,
        required: true
    },
    Profession: {
        type: String,
        required: true
    },
    Company: {
        type: String,
        required: true
    },
    EmpID: {
        type: Number,
        required: true
    },
    Role: {
        type: String,
        required: true
    },
})
module.exports = mongoose.model('User', CreateUser, 'User');