'use strict'

var User = require('./auth');
var auth = require('../En&DeCrypt');
var Boom = require('boom');

exports.loginValidate = function (request, reply) {
    var email = request.payload.Email;
    var password = request.payload.Password;
    var searchUser = {
        Email: email
    }
    User.findOne(searchUser, '-__v', function (err, user) {
        if (err) return reply(err);
        if (!user) return reply(Boom.unauthorized('Email Address incorrect'));
        if (user) {
            var authenticatedUser = auth.authcheck(password, user.Password);
            if (authenticatedUser) {
                delete user._doc.Password;
                // request.cookieAuth.set({ user: user, scope: user.Tag[0] });
                return reply({ "Status": "Success", "message": "Authenticated", "User": user });
            }
            return reply(Boom.unauthorized('password incorrect'));
        }
    })
};

exports.createNewUser = function (request, reply) {
    try {
        var data = request.payload;
        var email = request.payload.Email;
        var Password = request.payload.Password;
        var newUser = new User({
            FirstName: data.FirstName,
            LastName: data.LastName,
            Email: data.Email,
            PhoneNumber: data.PhoneNumber,
            Password: auth.encrypt(Password),
            Profession: data.Profession,
            Company: data.Company,
            EmpID: data.EmpID,
            Role: data.Role
        });
        var searchUser = {
            Email: email
        };
        User.findOne(searchUser, function (err, user) {
            if (err) return reply(err);
            if (user) return reply({
                message: 'User Already Exist'
            });
            newUser.save(function (err) {
                if (err) return reply(err);
                User.findOne(searchUser, { _id: 0, Password: 0, __v: 0 }, function (err, user) {
                    if (err) return reply(err);
                    if (user) {
                        request.cookieAuth.set({ user: user });
                        return reply({ user: user });
                    }
                })
            })
        });
    } catch (e) {
        return reply({ "status": "false", "message": e.message });
    }
};

exports.UpdateProfile = function (request, reply) {
    var searchUser = {
        Email: request.payload.Email
    };
    var fields2update = {
        FirstName: data.FirstName,
        LastName: data.LastName,
        Email: data.Email,
        PhoneNumber: data.PhoneNumber,
        Password: auth.encrypt(Password),
        Profession: data.Profession,
        Company: data.Company,
        EmpID: data.EmpID,
        Role: data.Role
    }
    User.findOne(searchUser, function (err, user) {
        if (err) return reply(err);
        if (user) {
            console.log(user);
            User.update({ Email: user.Email }, { $set: fields2update }, function (err) {
                if (err) return reply(err);
                User.findOne(searchUser, { _id: 0, Password: 0, __v: 0 }, function (err, user) {
                    if (err) return reply(err);
                    if (user) return reply({ user: user });
                })
            })
        }
    });
};